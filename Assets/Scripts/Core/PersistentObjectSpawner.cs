﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class PersistentObjectSpawner : MonoBehaviour
    {
        [SerializeField] GameObject persistantObjectPrefab;

        //remember objet between scenes
        static bool hasSpowned = false;
        private void Awake()
        {
            if (hasSpowned) return;

            SpawnPersistentObject();
        }

        private void SpawnPersistentObject()
        {
            GameObject persistentObject = Instantiate(persistantObjectPrefab);
            DontDestroyOnLoad(persistentObject);
        }
    }
}