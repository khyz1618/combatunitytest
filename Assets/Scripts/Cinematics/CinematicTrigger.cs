﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables; //~playable director stuff


/*
 * Remember - Intro Secuence - ignore raycast layer.
 */
namespace RPG.Cinematics
{

    public class CinematicTrigger : MonoBehaviour
    {
        private bool alreadyTriggered = false;
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player" && !alreadyTriggered)
            {
                GetComponent<PlayableDirector>().Play();
                alreadyTriggered = true;
            }
        }
    }
}
