﻿using UnityEngine;
using UnityEngine.Playables; //~playable director stuff
using RPG.Control;
using RPG.Core;

namespace RPG.Cinematics
{
    public class CinematicsControlRemover : MonoBehaviour
    {
        GameObject player;
        private void Start()
        {
            GetComponent<PlayableDirector>().stopped += EnableControl;
            GetComponent<PlayableDirector>().played += DisableControl ;
            player = GameObject.FindWithTag("Player");
          
        }
        void DisableControl(PlayableDirector pd)
        {            
            player.GetComponent<ActionScheduler>().CancelCurrentAction();
            player.GetComponent<PlayerController>().enabled = false;
            print("Disabled control");
        }
        void EnableControl(PlayableDirector pd)
        {
            print("Enabled control");
            player.GetComponent<PlayerController>().enabled = true;
        }
    }
} 